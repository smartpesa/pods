Pod::Spec.new do |s|  
    s.name              = 'MySDK'
    s.version           = '1.0.3'
    s.summary           = 'A really cool SDK that logs stuff.'
    s.homepage          = 'http://example.com/'

    s.author            = { 'Name' => 'sdk@example.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => 'https://bitbucket.org/smartpesa/pods/raw/master/MySDK/1.0.3/MySDK.zip' }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'MySDK.framework'

    s.dependency 'AlamofireSwiftyJSON'
end  
